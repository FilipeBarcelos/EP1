#include <fstream>
#include <stdlib.h>
#include "filtronegativo.hpp"

using namespace std;

// Implementando o método construtor padrão utilizando os métodos acessores da classe principal.
FiltroNegativo::FiltroNegativo() {
	setImagem("");
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setNumeroMagico("");
	setLargura(0);
	setAltura(0);
	setTamanhoCabecalho(0);
}
// Implementando o método destrutor padrão.
FiltroNegativo::~FiltroNegativo() {

}
// Implementando a função que aplica o filtro.
void FiltroNegativo::AplicaFiltro() {
	string nome = "";
	int i;
	
	cout << "Insira um nome para a imagem com filtro negativo: ";
	cin >> nome;
	cout << endl; 

	ofstream nova_imagem;
	nova_imagem.open(nome.c_str());

	for (i = 0; i < getTamanhoCabecalho(); i++) {
		nova_imagem <<  getImagem()[i]; 
		
	}
	nova_imagem << '\n';
	for (i = i+1; i < getTamanhoTotal(); i++) {
		nova_imagem << (unsigned char) (getNivelCor()-getImagem()[i]);
	}

	nova_imagem.close();

}