#include <fstream>
#include <stdlib.h>
#include "filtropolarizado.hpp"

using namespace std;

// Implementando o método construtor padrão utilizando os métodos acessores da classe principal.
FiltroPolarizado::FiltroPolarizado() {
	setImagem("");
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setNumeroMagico("");
	setLargura(0);
	setAltura(0);
	setTamanhoCabecalho(0);
}
// Implementando o método destrutor padrão.
FiltroPolarizado::~FiltroPolarizado() {

}
// Implementando a função que aplica o filtro.
void FiltroPolarizado::AplicaFiltro() {
	string nome = "";
	int i;
	
	cout << "Insira um nome para a imagem com filtro polarizado: ";
	cin >> nome;
	cout << endl; 

	ofstream nova_imagem;
	nova_imagem.open(nome.c_str());

	for (i = 0; i < getTamanhoCabecalho(); i++) {
		nova_imagem <<  getImagem()[i]; 
		
	}
	nova_imagem << '\n';
	for (i = i+1; i < getTamanhoTotal(); i++) {
		if ((unsigned char) getImagem()[i] < getNivelCor()/2) {
			nova_imagem << 0;	
		} else {
			nova_imagem << getNivelCor();	
		} 				 
	}

	nova_imagem.close();

}