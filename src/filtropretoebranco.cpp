#include <fstream>
#include <stdlib.h>
#include "filtropretoebranco.hpp"

using namespace std;

// Implementando o método construtor padrão utilizando os métodos acessores da classe principal.
FiltroPretoeBranco::FiltroPretoeBranco() {
	setImagem("");
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setNumeroMagico("");
	setLargura(0);
	setAltura(0);
	setTamanhoCabecalho(0);
}
// Implementando o método destrutor padrão.
FiltroPretoeBranco::~FiltroPretoeBranco() {

}
// Implementando a função que aplica o filtro.
void FiltroPretoeBranco::AplicaFiltro() {
	string nome = "";
	int i;
	int contador1 = 0;
	char imagem_filtro;
	string red;
	string green;
	string blue;

	cout << "Insira um nome para a imagem com filtro preto e branco: ";
	cin >> nome;
	cout << endl; 
	
	ofstream nova_imagem;
	nova_imagem.open(nome.c_str());

	for (i = 0; i < getTamanhoCabecalho(); i++) {
		nova_imagem << getImagem()[i]; 

	}
	nova_imagem << '\n';
	for (int j = i+1; j < getTamanhoTotal(); j++) {
		contador1++;
		if (contador1 == 1) {
			red += getImagem()[j];	
		} else

		if (contador1 == 2) {
			green += getImagem()[j];	
		} else {
			blue += getImagem()[j];
			contador1=0;
		} 				 
	} 
	for (i = 0; i < getTamanhoImagem(); i++) {
		imagem_filtro = (unsigned char) (0.299 * red[i]) + (0.587 * green[i]) + (0.144 * blue[i]);
		if (imagem_filtro > getNivelCor()){
            imagem_filtro = getNivelCor();
        }
		nova_imagem << (unsigned char) imagem_filtro;
		nova_imagem << (unsigned char) imagem_filtro;
		nova_imagem << (unsigned char) imagem_filtro;
	} 

	nova_imagem.close();

}