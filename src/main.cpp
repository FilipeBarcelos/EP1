#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "imagemppm.hpp"
#include "filtros.hpp"
#include "filtronegativo.hpp"
#include "filtropolarizado.hpp"
#include "filtropretoebranco.hpp"
#include "filtrodemedia.hpp"

using namespace std;

int main(int argc, char ** argv) {
int escolha;

cout << "Escolha 1 para aplicar o filtro negativo, 2 para aplicar o filtro polarizado, 3 para aplicar o filtro preto e branco ou 4 para aplicar o filtro de media: ";
cin >> escolha;
cout << endl;

// Criando o objeto.
try {
	if (escolha == 1) {
		FiltroNegativo * imagem = new FiltroNegativo();
		// Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
		imagem->AbrirImagem();
		imagem->ExtrairAtributos();
		imagem->ExtrairCabecalho();

		if (imagem->getNumeroMagico() != "P6") {
			cout << "Tipo de imagem inválido!" << endl;
			exit(1); 
		}
		else {
			imagem->AplicaFiltro();

			cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
			cout << "Largura: " << imagem->getLargura() << endl;
			cout << "Altura: " << imagem->getAltura() << endl;
			cout << "Nível máximo de cor:" << ' ';
			printf("%d \n", imagem->getNivelCor());

			// Desalocando a memória.
			delete (imagem);
		}		
	} else 

	if (escolha == 2) {
		FiltroPolarizado * imagem = new FiltroPolarizado();
		// Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
		imagem->AbrirImagem();
		imagem->ExtrairAtributos();
		imagem->ExtrairCabecalho();

		if (imagem->getNumeroMagico() != "P6") {
			cout << "Tipo de imagem inválido!" << endl;
			exit(1); 
		}
		else {
			imagem->AplicaFiltro();

			cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
			cout << "Largura: " << imagem->getLargura() << endl;
			cout << "Altura: " << imagem->getAltura() << endl;
			cout << "Nível máximo de cor:" << ' ';
			printf("%d \n", imagem->getNivelCor());
		
			// Desalocando a memória.
			delete (imagem);
		}		
	} else

	 if (escolha == 3) {
	 	FiltroPretoeBranco * imagem = new FiltroPretoeBranco();
		// Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
		imagem->AbrirImagem();
		imagem->ExtrairAtributos();
		imagem->ExtrairCabecalho();

		if (imagem->getNumeroMagico() != "P6") {
			cout << "Tipo de imagem inválido!" << endl;
			exit(1); 
		}
		else {
			imagem->AplicaFiltro();

			cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
			cout << "Largura: " << imagem->getLargura() << endl;
			cout << "Altura: " << imagem->getAltura() << endl;
			cout << "Nível máximo de cor:" << ' ';
			printf("%d \n", imagem->getNivelCor());

			// Desalocando a memória.
			delete (imagem);		
		}	 	
	 } else

	 if (escolha == 4) {	
		FiltroMedia * imagem = new FiltroMedia();
		// Abrindo a imagem, extraindo os atributos, extraindo o cabeçalho e aplicando o filtro.
		imagem->AbrirImagem();
		imagem->ExtrairAtributos();
		imagem->ExtrairCabecalho();

		if (imagem->getNumeroMagico() != "P6") {
			cout << "Tipo de imagem inválido!" << endl;
			exit(1); 
		}
		else {
			imagem->AplicaFiltro();

			cout << "Numero mágico: " << imagem->getNumeroMagico() << endl;
			cout << "Largura: " << imagem->getLargura() << endl;
			cout << "Altura: " << imagem->getAltura() << endl;
			cout << "Nível máximo de cor:" << ' ';
			printf("%d \n", imagem->getNivelCor());

			// Desalocando a memória.
			delete (imagem);
		}		
	}  else
	if ((escolha != 1) || (escolha != 2) || (escolha != 3) || (escolha != 4)) {
		throw(6);
	} 
}
catch (int captura) {
	cout << "Escolha inválida";
	cout << endl;

}

return 0;

}
