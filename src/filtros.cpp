#include <fstream>
#include <stdlib.h>
#include "filtros.hpp"

using namespace std;

// Implementando o método construtor padrão utilizando os métodos acessores da classe principal.
Filtros::Filtros() {
	setImagem("");
	setTamanhoTotal(0);
	setTamanhoImagem(0);
	setNumeroMagico("");
	setLargura(0);
	setAltura(0);
	setTamanhoCabecalho(0);
}
// Implementando o método destrutor padrão.
Filtros::~Filtros() {

}

