#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include "imagemppm.hpp"

using namespace std;

// Implementado o método construtor padrão.
ImagemPPM::ImagemPPM() {
	imagem = "";
	numero_magico = "";
	nivel_cor = 0;
	tamanho_total = 0;
	tamanho_imagem = 0;
	largura = 0;
	altura = 0;
	tamanho_cabecalho = 0;

}
// Implementando o método destrutor.
ImagemPPM::~ImagemPPM() {

}
// Implementando os métodos acessores.
string ImagemPPM::getImagem() {
	return imagem;
}
void ImagemPPM::setImagem(string imagem) {
	this-> imagem = imagem;
}
string ImagemPPM::getNumeroMagico() {
        return numero_magico;
}
void ImagemPPM::setNumeroMagico(string numero_magico) {
        this-> numero_magico = numero_magico;
}
unsigned char ImagemPPM::getNivelCor() {
        return nivel_cor;
}
void ImagemPPM::setNivelCor(unsigned char nivel_cor) {
        this-> nivel_cor = nivel_cor;
}
int ImagemPPM::getTamanhoTotal() {
        return tamanho_total;
}
void ImagemPPM::setTamanhoTotal(int tamanho_total) {
        this-> tamanho_total = tamanho_total;
}
int ImagemPPM::getTamanhoImagem() {
        return tamanho_imagem;
}
void ImagemPPM::setTamanhoImagem(int tamanho_imagem) {
        this-> tamanho_imagem = tamanho_imagem;
}
int ImagemPPM::getLargura() {
        return largura;
}
void ImagemPPM::setLargura(int largura) {
        this-> largura = largura;
}
int ImagemPPM::getAltura() {
        return altura;
}
void ImagemPPM::setAltura(int altura) {
        this-> altura = altura;
}
void ImagemPPM::setTamanhoCabecalho(int tamanho_cabecalho) {
        this-> tamanho_cabecalho = tamanho_cabecalho;
}
int ImagemPPM::getTamanhoCabecalho() {
        return tamanho_cabecalho;
}
// Implementando a função que abre a imagem e guarda todas as informações em forma de caracteres.
void ImagemPPM::AbrirImagem() {
	string imagem = "";
	string endereco = "";
	int contador = 0;
	
	cout << "Insira o endereço da imagem a ser filtrada: ";
	cin >> endereco;
	cout << endl;
 	
	ifstream inFile;
	inFile.open(endereco.c_str());
	
	if (inFile.fail()) {
	cerr << "O arquivo não foi encontrado" << endl;
	exit(1);
	}

        while (!inFile.eof()) {
                imagem+= inFile.get();
                contador++;

        }
  
	this-> imagem = imagem;
	tamanho_total = contador;

}
// Implementando a função de extrair os dados do cabeçalho, como: Número mágico e Nível máximo de cor.
void ImagemPPM::ExtrairAtributos() {
	string numero_magico;
	string comentario;
	string largura;
	string altura;
	string nivel_cor;
	int contador = 0;
	int tamanho_imagem = 0;

	for (int i = 0; i < tamanho_total; i++) {
		if (imagem[i] == '\n') {
			contador++;
		}
		if ((imagem[i] != '\n') && (contador == 0)) {
			numero_magico += imagem[i];
		} else
		
		 if (imagem[i] == '#') {
			comentario += imagem[i];
			contador = 1;
		}else

		if ((contador == 2) && (imagem[i] != ' ')) {
			largura += imagem[i];
		} else

		if ((contador == 2) && (imagem[i] == ' ')) {
			contador ++;
		} else

		if (contador == 3) {
                        altura += imagem[i];
                }else 

		if (contador == 4) {
			nivel_cor += imagem[i];
		} else
	
		if (imagem[i] == 5) {
			break;
		}
	
	}

	tamanho_imagem = atoi(largura.c_str())*atoi(altura.c_str());
	this-> numero_magico = numero_magico;
	this-> largura = atoi(largura.c_str());
    this-> altura = atoi(altura.c_str());
	this-> tamanho_imagem = tamanho_imagem;
    this-> nivel_cor = atoi(nivel_cor.c_str());   
}
// Implementando a função conta o tamanho do cabeçalho.
void ImagemPPM::ExtrairCabecalho() {
	int cabecalho = 0;
	int contador = 0;
	string comentario;

	for(int i=0; i < tamanho_total; i++) {
		if (imagem[i] == '\n') {
			contador++;
		}

		if (imagem[i] == '#') {
			comentario += imagem[i];
			contador = 1;
		} else 
		
		if (contador == 4) {
			break;
		}
		cabecalho++; 		
	}

	tamanho_cabecalho = cabecalho;
}