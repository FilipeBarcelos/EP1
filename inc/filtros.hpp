#ifndef FILTROS_HPP
#define FILTROS_HPP

#include <iostream>
#include <string>
#include "imagemppm.hpp"

using namespace std;

// Criando a classe filha NovaImagem.
class Filtros : public ImagemPPM {
	public:
	Filtros(); // Construtor padrão.
	~Filtros(); // Destrutor.
	// Função de aplicar o filtro.
	virtual void AplicaFiltro() = 0;
};

#endif
