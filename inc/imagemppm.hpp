#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP
#include <iostream>
#include <string>

using namespace std;

// Criando a classe principal.
class ImagemPPM {
        private:
        string imagem;
	string numero_magico;
	unsigned char nivel_cor;
        int tamanho_total;
        int tamanho_imagem;
        int largura;
        int altura;
        int tamanho_cabecalho;
        public:
        ImagemPPM(); // Construtor padrão.
        ~ImagemPPM(); // Destrutor.
	// Métodos acessores.
        string getImagem();
        void setImagem(string imagem);
        string getNumeroMagico();
        void setNumeroMagico(string numero_magico);
        unsigned char getNivelCor();
        void setNivelCor(unsigned char nivel_cor);
        int getTamanhoTotal();
        void setTamanhoTotal(int tamanho_total);
        int getTamanhoImagem();
        void setTamanhoImagem(int tamanho_imagem);
        int getLargura();
        void setLargura(int largura);
        int getAltura();
        void setAltura(int altura);
        int getTamanhoCabecalho();
        void setTamanhoCabecalho(int tamanho_cabecalho);
	// Funções de abrir a imagem e extrair os dados do cabeçalho.
        void AbrirImagem();
        void ExtrairAtributos();
        void ExtrairCabecalho();

};

#endif
