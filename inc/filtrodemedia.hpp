#ifndef FILTRODEMEDIA_HPP
#define FILTRODEMEDIA_HPP

#include <iostream>
#include <string>
#include "filtros.hpp"

using namespace std;

// Criando a classe filha NovaImagem.
class FiltroMedia : public Filtros {
	public:
	FiltroMedia(); // Construtor padrão.
	~FiltroMedia(); // Destrutor.
	// Função de aplicar o filtro.
	void AplicaFiltro();
};

#endif