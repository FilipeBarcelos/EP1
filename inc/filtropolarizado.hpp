#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include <iostream>
#include <string>
#include "filtros.hpp"

using namespace std;

// Criando a classe filha NovaImagem.
class FiltroPolarizado : public Filtros {
	public:
	FiltroPolarizado(); // Construtor padrão.
	~FiltroPolarizado(); // Destrutor.
	// Função de aplicar o filtro.
	void AplicaFiltro();
};

#endif