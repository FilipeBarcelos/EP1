#ifndef FILTROPRETOEBRANCO_HPP
#define FILTROPRETOEBRANCO_HPP

#include <iostream>
#include <string>
#include "filtros.hpp"

using namespace std;

// Criando a classe filha NovaImagem.
class FiltroPretoeBranco : public Filtros {
	public:
	FiltroPretoeBranco(); // Construtor padrão.
	~FiltroPretoeBranco(); // Destrutor.
	// Função de aplicar o filtro.
	void AplicaFiltro();
};

#endif