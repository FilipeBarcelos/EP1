Para executar o programa que utiliza um MAKEFILE, deve-se seguir os passos abaixo:

1-Crie as pastas obj e bin caso não existam.
2-Utilizando o terminal do linux siga para o local destino onde as pastas se encontram.
3-Execute o MAKEFILE com o comando "make" no próprio terminal.
4-Após a compilação execute o comando "make run".

Dentro do programa os passos são os seguintes:

1-Escolha um dos filtros sugeridos para a aplicação.
2-Insira um endereço da imagem do tipo selecionada. Exemplo: "doc/unb.ppm"
3-Escolha um nome para a nova imagem com filtro sempre adicionando ".ppm" ao final. Exemplo: "unb_negativo.ppm"
4-A imagem será criada na pasta do projeto.

Obs: A implementação do filtro de média não foi concluída com sucesso.

Após o encerramento do programa insira no terminal o comando "make clean" para a limpeza das pastas bin e obj.
